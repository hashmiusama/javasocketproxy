package edu.asupoly.ser421.server;

/**
 * @author hashmiusama
 */
public class ServerDetails {
    public static int localPort, serverPort, cacheSize=1024, delay, cacheStaleTime = 10000; //stale cache time 10s
    public static String serverAddress;
    public static String diskCacheLocation = "cache.ser";
}
