package edu.asupoly.ser421.caching;

import edu.asupoly.ser421.logging.Logger;

/**
 * @author hashmiusama
 */
public class CacheProvider {
    public static final LRUCache cache = LRUCache.getCache();

    public static String get(String key){
        System.out.println(CacheProvider.cache);
        return cache.get(key);
    }

    public static boolean put(String key, String value){
        if(value.length() > cache.getMaxSize()) return false;
        String oldValue = cache.get(key);
        if(value.equals(oldValue)){
            if(cache.getCurrentSize() > cache.getMaxSize()){
                synchronized (cache){
                    cache.remove(key);
                    cache.put(key, value);
                }
                put(key, value);
            }
            System.out.println(CacheProvider.cache);
            return false;
        }else{
            synchronized (cache) {
                cache.put(key, value);
            }
            if(oldValue!=null){
                synchronized (cache){
                    cache.setCurrentSize(cache.getCurrentSize() - oldValue.length() + value.length());
                }

            }else{
                synchronized (cache){
                    cache.setCurrentSize(cache.getCurrentSize()+value.length());
                }
            }
            put(key,value);
            System.out.println(CacheProvider.cache);
            return true;
        }
    }
}
