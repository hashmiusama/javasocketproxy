package edu.asupoly.ser421.testing;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author hashmiusama
 */
public class Test {
    public static void main(String args[]) throws IOException {
        ArrayList<String> a = new ArrayList<>();
        a.add("get /~adye/javatutorial/networking/sockets/clientServer.html");
        a.add("get /~adye/javatutorial/networking/sockets/readingWriting.html");
        a.add("get /~adye/javatutorial/networking/datagrams/index.html");
        a.add("get /~adye/javatutorial/networking/datagrams/definition.html");
        a.add("get /~adye/javatutorial/networking/datagrams/clientServer.html");
        a.add("get /~adye/javatutorial/networking/security/index.html");
        a.add("get /~adye/javatutorial/networking/security/intro.html");
        a.add("get /~adye/javatutorial/networking/datagrams/index.html");
        a.add("get /~adye/javatutorial/networking/datagrams/definition.html");
        a.add("get /~adye/javatutorial/networking/datagrams/clientServer.html");
        a.add("get /~adye/javatutorial/networking/security/writingSMgr.html");
        a.add("get /~adye/javatutorial/networking/security/installSMgr.html");
        a.add("get /~adye/javatutorial/networking/security/more.html");
        a.add("get /~adye/javatutorial/networking/end.html");
        a.add("get /~adye/javatutorial/networking/security/installSMgr.html");
        a.add("get /~adye/javatutorial/networking/security/writingSMgr.html");
        a.add("get /adasdas");
        a.add("get /adasdas1");
        a.add("get /adasdas2");
        a.add("get /adasdas3");
        a.add("get /adasdas4");
        a.add("get /adasdas5");

        for (String anA : a) {
            Socket socket = new Socket("localhost", 8000);
            PrintWriter writer = new PrintWriter(socket.getOutputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            System.out.println(reader.readLine());
            System.out.println(reader.readLine());
            writer.write(anA+"\n");
            writer.flush();
            String returns = "";
            while (true) {
                returns = reader.readLine();
                System.out.println(returns);
                if (returns.equals("end")) {
                    break;
                }
            }
            socket.close();
        }
    }
}
