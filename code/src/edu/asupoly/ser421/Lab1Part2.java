package edu.asupoly.ser421;

import edu.asupoly.ser421.server.ServerDetails;
import edu.asupoly.ser421.socket.SocketListener;

import java.io.IOException;

/**
 * @author hashmiusama
 */
public class Lab1Part2 {
    public static void main(String[] args){
        //0 localport - the port to run this server on
        //1 server - hostname/ip of target server
        //2 port - port of target server
        //3 cache size - max size of in-cache memory
        //4 delay - delay in each connection
        //5 location of in memory cache on disk
        //6 time period in MS to remove stale objects in cache
        if(args.length < 5){
            System.out.println("Enter required arguments please.");
            System.exit(0);
        }else{
            try{
                ServerDetails.localPort = Integer.parseInt(args[0]);
                ServerDetails.serverAddress = args[1];
                ServerDetails.serverPort = Integer.parseInt(args[2]);
                ServerDetails.cacheSize = Integer.parseInt(args[3]);
                ServerDetails.delay = Integer.parseInt(args[4]);
                if(args.length == 6) ServerDetails.diskCacheLocation = args[5];
                if(args.length == 7) ServerDetails.cacheStaleTime = Integer.parseInt(args[6]);
            } catch(NumberFormatException ne){
                System.out.println("Enter required arguments correctly please.");
                System.exit(0);
            }
            SocketListener listener = null;
            while(listener == null){
                try {
                    listener = new SocketListener();
                    listener.listen();
                } catch (IOException e) {
                    System.out.println("Incrementing port by 1 and trying again.");
                    ServerDetails.localPort++;
                }
            }
        }
    }
}
