package edu.asupoly.ser421.http;

import edu.asupoly.ser421.caching.CacheProvider;
import edu.asupoly.ser421.logging.Logger;
import edu.asupoly.ser421.server.ServerDetails;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author hashmiusama
 */
public class HTTPGetProcessor {
    private String filename;
    public HTTPGetProcessor(String filename){
        this.filename = filename;
    }
    public String getFileFromServer() {
        String data = CacheProvider.get(filename);
        String inputLine;
        StringBuilder output = new StringBuilder();
        if(data != null){
            Logger.log("Got from cache.");
            return data;
        }else{
            URL url = null;
            try {
                url = new URL(ServerDetails.serverAddress);
                url = new URL(url.getProtocol(), url.getHost(), url.getPort(), this.filename);
		System.out.println("The url is: "+url);
            } catch (MalformedURLException e) {
                Logger.log("I am a malformed connection.");
                e.printStackTrace();
            }
            try{
                assert url != null;
                URLConnection yc = url.openConnection();
                BufferedReader in =  new BufferedReader(new InputStreamReader(yc.getInputStream()));
                while ((inputLine = in.readLine()) != null)
                    output.append(inputLine).append("\n");
                in.close();

                if(!output.toString().equals(""))
                    CacheProvider.put(filename, output.toString());

                Logger.log("" + CacheProvider.cache);

            }catch(FileNotFoundException e){
                Logger.log("I am coming from openConnection");
                Logger.log(e.getLocalizedMessage());
                return "";
            } catch (IOException e) {
                Logger.log("I am coming from input reader");
                Logger.log(e.getLocalizedMessage());
                if(e.getLocalizedMessage().contains("HTTP response code")){
                    return e.getLocalizedMessage();
                }
                return "";
            }
            return output.toString();
        }
    }
}
