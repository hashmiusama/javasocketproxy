1. Please use java8 to run the program.

2. For the purpose of making the code compilable and runnable using one command, 2 shell scripts have been made.
	compile.sh will compile the code.
	run.sh will run the code.
		./compile.sh
		./run.sh <proxyServerPort> <HTTPServer> <HTTPServerPort> <cacheSize> <Delay>
	example: ./run.sh 8000 http://hepunx.rl.ac.uk 80 20 1000
	if you wish to run the code without using run.sh, make sure you are in the "code" directory
	then run using command
		java -cp .:src/ edu.asupoly.ser421.Lab1Part2 <proxyServerPort> <HTTPServer> <HTTPServerPort> <cacheSize> <Delay>


		
Note: the assignment has implemented the bonus points as well so this can also be used to pass as extra arguments for filename and staleCache time
	
	java -cp .:src/ edu.asupoly.ser421.Lab1Part2 <proxyServerPort> <HTTPServer> <HTTPServerPort> <cacheSize in KBs> <Delay in MS> <cacheOnDiskFileLocation> <staleCaheRefresh Time in MS>
	example: java -cp .:src/ edu.asupoly.ser421.Lab1Part2 8000 http://hepunx.rl.ac.uk 80 20 1000 cache.ser 10000
	this can be run easily using 
		./run.sh <proxyServerPort> <HTTPServer> <HTTPServerPort> <cacheSize in KBs> <Delay in MS> <cacheOnDiskFileLocation> <staleCaheRefresh Time in MS>

note2: please use .ser as file extension for cache as this is a serialized version of the cache object that java deals with.
	default cache filename is "cache.ser".


4. A test code has also been implemented that assumes the server to be http://hepunx.rl.ac.uk and gets the html from the server.
	runTest.sh can be run as it is using ./runTest.sh in the "code" directory but make sure that the server was started using
	./run.sh 8000 http://hepunx.rl.ac.uk 80 20 1000 cache.ser 10000
	or other appropriate arguments.	

NOTE: make sure that all .sh files are granted execute permission
